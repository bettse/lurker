var WebClient = require('@slack/client').WebClient;

var RtmClient = require('@slack/client').RtmClient;
var CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS;
var RTM_EVENTS = require('@slack/client').RTM_EVENTS;

var token = process.env.SLACK_BOT_TOKEN || '';

var rtm = new RtmClient(token);
var web = new WebClient(token);

// The client will emit an RTM.AUTHENTICATED event on successful connection, with the `rtm.start` payload if you want to cache it 
rtm.on(CLIENT_EVENTS.RTM.AUTHENTICATED, function (rtmStartData) {
	console.log(`Logged in as ${rtmStartData.self.name} of team ${rtmStartData.team.name}, but not yet connected to a channel`);
});

rtm.on(RTM_EVENTS.MESSAGE, function(message) {

	if(message.text && message.text.match(/Eric/) !== null) {
	  console.log('Reacting to message with lurk', message.text)
		web.reactions.add("lurk", {
				channel: message.channel,
				timestamp: message.ts
		});
	} else {
	  console.log('Ignoring message', message.text)
  }

});

rtm.start();
